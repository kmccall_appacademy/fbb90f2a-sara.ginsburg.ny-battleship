class Board
  DISPLAY_HASH = {
      :x => "x",
      :s => "s",
      nil => " "
  }
  attr_accessor :grid, :x, :s

  def initialize(grid = Board.default_grid)
    @grid = grid

  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos,val)
    row, col = pos
    @grid[row][col] = val
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}

  end

  def count
    @grid.flatten.count(:s)
  end

  def empty?(pos = nil)
    if pos
      self[pos].nil?
    else
      count == 0
    end
  end

  def full?
    @grid.flatten.count(nil) == 0
  end

  def populate_grid(count = 10)
    count.times {place_random_ship}
  end

  def in_range?(pos)
  end

  def place_random_ship
    raise "place is full" if full?
    pos = rand(@grid.length), rand(@grid.length)

    until empty?(pos)
      pos = rand(@grid.length), rand(@grid.length)
    end

    self[pos] = :s

  end

  def won?
    @grid.flatten.count(:s) == 0
  end

  def display
    header = "  " + (0..9).to_a.join(" ")
    puts header
    @grid.each_with_index do |row, i|
    chars = row.map {|el| DISPLAY_HASH[el]}.join(" ")
    puts "#{i} #{chars}"
    end
  end

end
