require_relative "board"
require_relative "player"

class BattleshipGame

  attr_reader :board, :player, :game, :x

  def initialize(player = HumanPlayer.new("Sara"), board = Board.new)
    @player = player
    @board = board
  end

  def attack(pos)
    @board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    @board.won?
  end

  def play
    board.populate_grid
    until game_over?
      play_turn
    end
  end

  def play_turn
    @board.display
    pos = @player.get_play
    attack(pos)
  end





end


if __FILE__ == $PROGRAM_NAME

  BattleshipGame.new.play

end



# class Battleship
#
#   def initialize(player1, player2, board)
#       @player1 = player1
#       @player2 = player2
#       @board = board
#       @players = [player1, player2]
#   end
#
#   def switch_players
#      @players = @players.rotate
#   end
#
#   def attack
#   end
#
#   def play
#     #   loop while board is not won
#     until board.won?
#
#     end
#   end
#
#   def game_over?
#   end
#
#   def play_turn
#   end
#   end
#
#   board = Board.new
#   board.grid.each { |row| p row }
#
# end
