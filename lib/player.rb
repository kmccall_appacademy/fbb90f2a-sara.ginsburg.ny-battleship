class HumanPlayer

  attr_reader :name

  def initialize(name)
    @name = name
  end

  def get_play
    puts "#{name}, it is your turn, choose a position to place an 'x' (example: (1,3))"
    gets.chomp.split(",").map(&:to_i)
  end

end
